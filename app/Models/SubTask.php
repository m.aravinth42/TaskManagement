<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use DB;

/**
 * Class Countries
 * 
 * @property int $id
 * @property string|null $main_task_id
 * @property string|null $title
 * @property string $status
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class SubTask extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;
	protected $table = 'sub_task';

    protected $primaryKey = 'id';
	protected $casts = [
		'main_task_id' => 'int',
		'user_id' => 'int',
		'due_date'=>'datetime:Y-m-d',
		'created_at'=>'datetime:Y-m-d H:i:s',
		'updated_at'=>'datetime:Y-m-d H:i:s',
	];

	protected $fillable = [
		'main_task_id',
		'user_id',
		'title',
		'due_date',
		'status'
	];

}
