<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email'=>'required|string|exists:users,email',
            'password'=>'required|string'
        ]);

        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            //$response = ['message'=>'Invalid login credentials'];
            $user = User::where('email', $request->email)->first();
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken('client_token')->accessToken;
                    $response = ['token' => $token];
                    return response($response, 200);
                } else {
                    $response = ["message" => "Password mismatch"];
                    return response($response, 422);
                }
            } else {
                $response = ["message" =>'User does not exist'];
                return response($response, 422);
            }
        }

        
    }

    public function register(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email'=>'required|email|unique:users',
            'password'=>'required|string',
        ]);
        
        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $password = Hash::make($request->input('password'));
            $request['password'] = $password;
            $ins = User::create($request->input());

            return response()->json(array(
                'success' => true,
                'message'=>'User has been added successfully',
            ), 200); // 400 being the HTTP code for an invalid request.  
        }
    }

    // public function logout (Request $request) {
    //     $accessToken = Auth::user()->token();
    //     $token= $request->user()->tokens->find($accessToken);
    //     $token->revoke();
    //     return response()->json(['status' => true,'message' =>"You have been successfully logged out"]);
    // }

}
