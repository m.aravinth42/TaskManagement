<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
use App\Models\SubTask;

class SubTaskController extends Controller
{
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title'=>'required|string|unique:sub_task',
            'main_task_id'=>'required|int',
            'due_date'=>'required|string'
        ]);

        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $ins_data = $request->all();
            $user = Auth::guard('api')->user()->id;
            $ins_data['user_id'] = $user;
            $ins = SubTask::create($ins_data);

            return response()->json(array(
                'success' => true,
                'message'=>'Sub task has been added successfully',
                'Sub_task_id'=>$ins->id
            ), 200); // 400 being the HTTP code for an invalid request.  
        }
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'sub_task_id'=>'required|int',
            'status'=>'required|int',
        ]);

        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $req = $request->all();

            $sub = SubTask::where('id', $request->input('sub_task_id'));
            if($sub->count()>0){
                $sub->update(['status'=>$req['status']]);
                
                return response()->json(array(
                    'success' => true,
                    'message'=>'Sub task has been updated successfully',
                ), 200); // 400 being the HTTP code for an invalid request.  
            }
            else{
                return response()->json(array(
                    'success' => false,
                    'errors' => 'Task not found'
                ), 400); // 400 being the HTTP code for an invalid request.  
            }
        }
    }

    public function delete(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'sub_task_id'=>'required|int',
        ]);
        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $sub = SubTask::where('id', $request->input('sub_task_id'));
            if($sub->count()>0){
                $sub->delete();
                
                return response()->json(array(
                    'success' => true,
                    'message'=>'Sub task has been deleted successfully',
                ), 200); // 400 being the HTTP code for an invalid request.  
            }
            else{
                return response()->json(array(
                    'success' => false,
                    'errors' => 'Task not found'
                ), 400); // 400 being the HTTP code for an invalid request.  
            }
           
        }
    }

    public function list(Request $request)
    {
        $user = Auth::guard('api')->user()->id;
        $date_filter = $request->input('date');
        $title_filter = $request->input('title');
        $master_task_id= $request->input('master_task_id');
        
        $res = SubTask::where(function ($query) use ($user, $date_filter, $title_filter, $master_task_id) {

                            $query = $query->where('user_id', $user);

                            $query->when($date_filter != "", function($query) use ($date_filter){

                                switch ($date_filter) {
                                    case 'Today':
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(due_date) = UNIX_TIMESTAMP(CURDATE()))');
                                    break;

                                    case 'This Week':
                                        //monday - sunday
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(SUBDATE(CURDATE(), weekday(CURDATE()))) <= UNIX_TIMESTAMP(CURDATE()) AND UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 6 DAY, weekday(CURDATE()))) >= UNIX_TIMESTAMP(CURDATE()))');
                                    break;

                                    case 'Next Week':
                                        //monday - sunday
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 7 DAY, weekday(CURDATE()))) <= UNIX_TIMESTAMP(due_date) AND UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 13 DAY, weekday(CURDATE()))) >= UNIX_TIMESTAMP(due_date))');
                                    break;
                                    case 'Overdue':
                                        //past date with status 0
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(due_date) < '.strtotime(date('y-m-d')).' AND status="0")');
                                    break;
                                }
                                
                                return $q;
                            });

                            $query->when($title_filter != "", function($query) use ($title_filter){
                                $q =  $query->where('title',$title_filter);
                                return $q;
                            });

                            $query->when($master_task_id != "", function($query) use ($master_task_id){
                                $q =  $query->where('master_task_id',$master_task_id);
                                return $q;
                            });
                        })
                        ->orderBy('due_date', 'ASC')
                        ->get();

        return $res->toJson();
    }
}
