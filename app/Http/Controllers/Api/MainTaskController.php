<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\MainTask;
use App\Models\SubTask;

class MainTaskController extends Controller
{
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title'=>'required|string|unique:main_task',
            'due_date'=>'required|string'
        ]);

        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $ins_data = $request->all();
            $user = Auth::guard('api')->user()->id;
            $ins_data['user_id'] = $user;

            $ins = MainTask::create($ins_data);

            return response()->json(array(
                'success' => true,
                'message'=>'Main task has been added successfully',
                'Main_task_id'=>$ins->id
            ), 200); // 400 being the HTTP code for an invalid request.  
        }
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'main_task_id'=>'required|int',
            'status'=>'required|int',
        ]);

        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $req = $request->all();
            $sub = SubTask::where('main_task_id', $req['main_task_id'])->update(['status'=>$req['status']]);
            
            $main = MainTask::where('id',$request->input('main_task_id'));
            if($main->count()>0){
                $main->update(['status' => $req['status']]);
                return response()->json(array(
                    'success' => true,
                    'message'=>'Main task has been updated successfully',
                ), 200); // 400 being the HTTP code for an invalid request.  
            }
            else{
                return response()->json(array(
                    'success' => false,
                    'errors' => 'Task not found'
                ), 400); // 400 being the HTTP code for an invalid request.  
            }
        }
    }

    public function delete(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'main_task_id'=>'required|int',
        ]);
        if ($validate->fails()) { 
            return response()->json(array(
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.  
        }
        else{
            $sub = SubTask::where('main_task_id', $request->input('main_task_id'));
            if($sub->count()>0){
                $sub->delete();
            }
            
            $main = MainTask::where('id',$request->input('main_task_id'));
            if($main->count()>0){
                $main->delete();
                return response()->json(array(
                    'success' => true,
                    'message'=>'Main task has been deleted successfully',
                ), 200); // 400 being the HTTP code for an invalid request.  
            }
            else{
                return response()->json(array(
                    'success' => false,
                    'errors' => 'Task not found'
                ), 400); // 400 being the HTTP code for an invalid request.  
            }
            
        }
    }

    public function list(Request $request)
    {
        $user = Auth::guard('api')->user()->id;
        $date_filter = $request->input('date');
        $title_filter = $request->input('title');

        
        $res = MainTask::where(function ($query) use ($user, $date_filter, $title_filter) {

                            $query = $query->where('user_id', $user);

                            $query->when($date_filter != "", function($query) use ($date_filter){

                                switch (trim($date_filter)) {
                                    case 'Today':
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(due_date) = UNIX_TIMESTAMP(CURDATE()))');
                                    break;

                                    case 'This Week':
                                        //monday - sunday
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(SUBDATE(CURDATE(), weekday(CURDATE()))) <= UNIX_TIMESTAMP(CURDATE()) AND UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 6 DAY, weekday(CURDATE()))) >= UNIX_TIMESTAMP(CURDATE()))');
                                    break;

                                    case 'Next Week':
                                        //monday - sunday
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 7 DAY, weekday(CURDATE()))) <= UNIX_TIMESTAMP(due_date) AND UNIX_TIMESTAMP(SUBDATE(CURDATE() + INTERVAL 13 DAY, weekday(CURDATE()))) >= UNIX_TIMESTAMP(due_date))');
                                    break;
                                    case 'Overdue':
                                        //past date with status 0
                                        $q =  $query->whereRaw('(UNIX_TIMESTAMP(due_date) < '.strtotime(date('y-m-d')).' AND status="0")');
                                    break;
                                }
                                
                                return $q;
                            });

                            $query->when($title_filter != "", function($query) use ($title_filter){
                                $q =  $query->where('title',$title_filter);
                                return $q;
                            });
                        })
                        ->orderBy('due_date', 'ASC')
                        ->get();

        return $res->toJson();
    }
}
