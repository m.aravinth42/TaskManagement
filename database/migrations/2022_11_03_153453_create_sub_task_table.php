<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_task', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_task_id');
            $table->integer('user_id');
            $table->string('title');
            $table->date('due_date');
            $table->enum('status', ['0', '1'])->default('1')->comment('\'0\'-pending,\'1\'-completed');
            // $table->dateTime('created_at')->useCurrent();
            // $table->dateTime('updated_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_task');
    }
};
