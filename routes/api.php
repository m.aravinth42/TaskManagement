<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\MainTaskController;
use App\Http\Controllers\Api\SubTaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login',[LoginController::class, 'login'])->name('login'); 
Route::post('register',[LoginController::class, 'register'])->name('register'); 


Route::middleware(['auth:api'])->group(function(){
    Route::any('/create_main_task',[MainTaskController::class, 'create']); 
    Route::any('/update_main_task',[MainTaskController::class, 'update']); 
    Route::any('/list_main_task',[MainTaskController::class, 'list']); 
    Route::any('/delete_main_task',[MainTaskController::class, 'delete']); 


    Route::any('/create_sub_task',[SubTaskController::class, 'create']); 
    Route::any('/update_sub_task',[SubTaskController::class, 'update']); 
    Route::any('/list_sub_task',[SubTaskController::class, 'list']); 
    Route::any('/delete_sub_task',[SubTaskController::class, 'delete']); 

});
